﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour {

    public GameObject sword;
    public Player player;

    public float requiredSliceSpeed;
    public bool isSlicing;
    public float staminaDrainMultiplier = 1.0f;
    private bool isExhausted;

    private float horizontalMouseSpeed, verticalMouseSpeed;
    private float angle, lastAngle;
    private Vector3 lastDir;

    private LineRenderer lineRenderer;

    void Awake() {
        isExhausted = false;
        lineRenderer = GetComponent<LineRenderer>();
        lastDir = Vector2.zero;
        sword.SetActive(false);
    }

    void Update() {
        CheckMouseInput();
        if (sword.activeSelf) {
            Vector3 mousePos =
                Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos = new Vector3(
                mousePos.x + Mathf.Cos((lastAngle + 90.0f) * Mathf.Deg2Rad),
                mousePos.y + Mathf.Sin((lastAngle + 90.0f) * Mathf.Deg2Rad),
                0.0f
            );

            Vector3 dir = lastDir - mousePos;
            lineRenderer.SetPosition(0, lastDir);
            lineRenderer.SetPosition(1, mousePos);
            angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90.0f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            horizontalMouseSpeed = Input.GetAxis("Mouse X");
            verticalMouseSpeed = Input.GetAxis("Mouse Y");

            if (Mathf.Abs(horizontalMouseSpeed) >= requiredSliceSpeed ||
                Mathf.Abs(verticalMouseSpeed) >= requiredSliceSpeed) {
                isSlicing = true;
                if (player.stamina > 0.5f) {
                    player.stamina -= (
                        Mathf.Abs(horizontalMouseSpeed) +
                        Mathf.Abs(verticalMouseSpeed)
                    ) * staminaDrainMultiplier;
                }
                else {
                    isExhausted = true;
                    player.speed *= player.slowdownMultipler;
                }
                GameControl.instance.SetStaminaSlider();
            }
            else {
                isSlicing = false;
            }
        }
    }

    private void CheckMouseInput() {
        if (player.stamina >= 100.0f && isExhausted) {
            isExhausted = false;
            player.speed /= player.slowdownMultipler;
        }

        if (Input.GetMouseButtonUp(0) || isExhausted) {
            sword.SetActive(false);
            lastAngle = angle;

            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.zero);
        }

        if (!isExhausted && Time.timeScale > 0) {
            if (Input.GetMouseButtonDown(0)) {
                lastDir = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                lastDir = new Vector3(lastDir.x, lastDir.y, 0.0f);
            }
            if (Input.GetMouseButton(0)) {
                sword.SetActive(true);
            }
        }
    }
}
