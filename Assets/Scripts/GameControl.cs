﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Pathfinding;

public class GameControl : MonoBehaviour {

    public static GameControl instance;

    public Player player;
    public Slider healthSlider;
    public Slider staminaSlider;

    public Text enemiesLeftText;
    public Button spawnWaveButton;

    public int numberEnemiesAlive;
    public int numberEnemiesOnScreen;
    public int numberEnemiesCharging;

    public GameObject enemyObjPrefab;
    private float enemySpawnX, enemySpawnY;
    private List<GameObject> enemies;

    public bool gameOver = false;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != null) {
            Destroy(gameObject);
        }
    }

    void Update() {
        if (gameOver && Input.GetKeyDown(KeyCode.R)) {
            gameOver = false;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (numberEnemiesAlive == 0) {
            if (SceneManager.GetActiveScene().name.Equals("lvl3")) {
                MenuControl.instance.ActivateYouWin();
            }
            else {
                MenuControl.instance.ActivateYouPassed();
            }
            SoundPlayer.instance.StopMusic();
            if (Input.GetMouseButtonDown(0)) {
                if (SceneManager.GetActiveScene().name.Equals("lvl1")) {
                    SceneManager.LoadScene("lvl2");
                }
                else if (SceneManager.GetActiveScene().name.Equals("lvl2")) {
                    SceneManager.LoadScene("lvl3");
                }
                else {
                    Application.Quit();
                }
            }
        }
    }

    private void GenerateSpawnArea() {
        int spawnSide = Random.Range(0, 5);
        if (spawnSide == 0) { //up
            enemySpawnX = Random.Range(-10.0f, 10.0f);
            enemySpawnY = 16.0f;
        }
        else if (spawnSide == 1) { //down
            enemySpawnX = Random.Range(-10.0f, 10.0f);
            enemySpawnY = -16.0f;
        }
        else if (spawnSide == 2) { //left
            enemySpawnX = -14.0f;
            enemySpawnY = -Random.Range(-14.0f, 14.0f);
        }
        else {
            enemySpawnX = 14.0f;
            enemySpawnY = -Random.Range(-14.0f, 14.0f);
        }
    }

    private GameObject SpawnEnemy() {
        GenerateSpawnArea();
        Vector2 initEnemyPos = new Vector3(enemySpawnX, enemySpawnY, 0);
        GameObject enemyObj =
            Instantiate(enemyObjPrefab, initEnemyPos, new Quaternion());
        AIDestinationSetter destSetter =
            enemyObj.GetComponent<AIDestinationSetter>();

        destSetter.target = player.transform;
        enemies.Add(enemyObj);

        return enemyObj;
    }

    public void SetNumEnemiesLeft() {
        if (SceneManager.GetActiveScene().name.Equals("lvl1")) {
            numberEnemiesAlive = 50;
            numberEnemiesOnScreen = 5;
        }
        else if (SceneManager.GetActiveScene().name.Equals("lvl2")) {
            numberEnemiesAlive = 100;
            numberEnemiesOnScreen = 10;
        }
        else {
            numberEnemiesAlive = 300;
            numberEnemiesOnScreen = 20;
        }
        enemiesLeftText.text = "Enemies Left\n" + numberEnemiesAlive;
    }

    public void BeginGame() {
        enemies = new List<GameObject>();
        SetNumEnemiesLeft();
        for (int i = 0; i < numberEnemiesOnScreen; i++) {
            GameObject enemyObj = SpawnEnemy();
        }
        if (SceneManager.GetActiveScene().name.Equals("lvl1")) {
            SoundPlayer.instance.PlayTrackOne();
        }
        else if (SceneManager.GetActiveScene().name.Equals("lvl2")) {
            SoundPlayer.instance.PlayTrackTwo();
        }
        else {
            SoundPlayer.instance.PlayTrackThree();
        }
        spawnWaveButton.gameObject.SetActive(false);
    }

    public void KillEnemy(GameObject enemy) {
        enemies.Remove(enemy);
        numberEnemiesAlive--;
        numberEnemiesOnScreen--;

        if (numberEnemiesOnScreen < numberEnemiesAlive) {
            GameObject newEnemyObj = SpawnEnemy();
            numberEnemiesOnScreen++;
        }
        enemiesLeftText.text = "Enemies Left\n" + numberEnemiesAlive;
    }

    public void SetHealthSlider() {
        healthSlider.value = player.health / 100.0f;
    }

    public void SetStaminaSlider() {
        staminaSlider.value = player.stamina / 100.0f;
    }
}
