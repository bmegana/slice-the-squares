﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float speed = 5.0f;

    public float health = 100.0f;
    public float healthDepletionRate = 0.375f;

    public float stamina = 100.0f;
    public float staminaRecoveryAmount = 0.5f;
    public float slowdownMultipler = 0.375f;
    private SpriteRenderer spRen;

    private float horizontalMove, verticalMove;
    private Rigidbody2D rb;

    void Awake() {
        spRen = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        GameControl.instance.player = this;
    }

    void Start () {
        GameControl.instance.SetHealthSlider();
        GameControl.instance.SetStaminaSlider();

        GameControl.instance.SetNumEnemiesLeft();
        MenuControl.instance.ActivatePlayerInfoPanel();
    }
	
	void Update () {
        horizontalMove = Input.GetAxis("Horizontal") * speed;
        verticalMove = Input.GetAxis("Vertical") * speed;
        RecoverStamina();

        if (stamina <= 0.5f) {
            spRen.color = Color.yellow;
        }
        if (health <= 0.5f) {
            Die();
        }
    }

    void FixedUpdate() {
        rb.velocity = new Vector2(horizontalMove, verticalMove);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        GameObject entity = collision.gameObject;
        if (entity.CompareTag("Enemy")) {
            Vector3 enemyForce = entity.GetComponent<Rigidbody2D>().velocity;
            rb.AddForce(enemyForce, ForceMode2D.Impulse);

            health -= healthDepletionRate;
            GameControl.instance.SetHealthSlider();
        }
    }

    void OnCollisionStay2D(Collision2D collision) {
        GameObject entity = collision.gameObject;
        if (entity.CompareTag("Enemy")) {
            health -= healthDepletionRate;
            GameControl.instance.SetHealthSlider();
        }
    }

    private void RecoverStamina() {
        if (stamina < 100.0f) {
            stamina += staminaRecoveryAmount;
            GameControl.instance.SetStaminaSlider();
        }
        if (stamina >= 99.9f) {
            spRen.color = Color.white;
        }
    }

    public Vector2 GetPosition() {
        return gameObject.transform.position;
    }

    private void Die() {
        SoundPlayer.instance.StopMusic();
        SoundPlayer.instance.PlayPlayerDeathSound();
        MenuControl.instance.ActivateGameOverMenu();
        GameControl.instance.gameOver = true;

        gameObject.SetActive(false);
    }
}
