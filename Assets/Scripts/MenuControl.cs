﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

    public static MenuControl instance;

    public EventSystem eventSystem;

    public GameObject mainMenuPanel;
    public GameObject gameOverPanel;
    public GameObject controlsPanel;

    public GameObject optionsPanel;
    public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;

    public GameObject pausePanel;
    public GameObject youPassedPanel;
    public GameObject youWinPanel;
    public GameObject quitPanel;
    public GameObject playerInfoPanel;
    public string levelToGoNext;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != null) {
            Destroy(this);
        }
    }

    private void Start() {
        DeactivateAllMenuPanels();
        if (!IsPlayingGame()) {
            ActivateMainMenu();
        }

        musicVolumeSlider.value =
            PlayerPrefs.GetFloat("Music Volume", 0.5f);
        sfxVolumeSlider.value =
            PlayerPrefs.GetFloat("SFX Volume", 0.5f);

        SetMusicVolume();
        SetSFXVolume();
    }

    private void Update() {
        if (IsPlayingGame()) {
            if (Input.GetButtonDown("Pause")) {
                if (Time.timeScale == 1) {
                    ActivatePauseMenu();
                }
                else {
                    DeactivatePauseMenu();
                }
            }
        }
    }

    public void QuitGame() {
        Application.Quit();
    }

    public void LevelLoader(string levelName) {
        DeactivateAllMenuPanels();
        SceneManager.LoadScene(levelName);
    }

    public void LoadMainMenuScene() {
        Time.timeScale = 1;
        DeactivateAllMenuPanels();        
        SceneManager.LoadScene("MainMenu");
    }

    public void ChangePanel(string panelName) {
        DeactivateAllMenuPanels();
        if (panelName.Equals("MainMenu")) {
            ActivateMainMenu();
        }
        else if (panelName.Equals("Controls")) {
            controlsPanel.SetActive(true);
            AutoSelectButton(controlsPanel, "BackButton");
        }
        else if (panelName.Equals("Options")) {
            optionsPanel.SetActive(true);
            AutoSelectButton(optionsPanel, "BackButton");
        }
        else if (panelName.Equals("Game Over")) {
            gameOverPanel.SetActive(true);
            AutoSelectButton(gameOverPanel, "Continue");
        }
        else if (panelName.Equals("You Passed")) {
            ActivateYouPassed();
        }
        else if (panelName.Equals("Quit")) {
            quitPanel.SetActive(true);
            AutoSelectButton(quitPanel, "NoButton");
        }
    }

    public void ActivateMainMenu() {
        mainMenuPanel.SetActive(true);
        AutoSelectButton(mainMenuPanel, "StartGameButton");
    }

    public void SetMusicVolume() {
        SoundPlayer.instance.SetMusicVolume(musicVolumeSlider.value);
    }

    public void SetSFXVolume() {
        SoundPlayer.instance.SetSoundFXVolume(sfxVolumeSlider.value);
    }

    public void ActivatePauseMenu() {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
        AutoSelectButton(pausePanel, "ContinueButton");
    }

    public void DeactivatePauseMenu() {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void ActivateYouPassed() {
        youPassedPanel.SetActive(true);
    }

    public void ActivateYouWin() {
        DeactivateAllMenuPanels();
        youWinPanel.SetActive(true);
    }

    public void ActivateGameOverMenu() {
        gameOverPanel.SetActive(true);
    }

    public void ActivatePlayerInfoPanel() {
        playerInfoPanel.SetActive(true);
    }

    public void DeactivatePlayerInfoPanel() {
        playerInfoPanel.SetActive(false);
    }

    public void DeactivateAllMenuPanels() {
        mainMenuPanel.SetActive(false);
        controlsPanel.SetActive(false);
        optionsPanel.SetActive(false);
        pausePanel.SetActive(false);
        gameOverPanel.SetActive(false);
        quitPanel.SetActive(false);
    }

    public void BackToPreviousPanel() {
        DeactivateAllMenuPanels();
        if (!IsPlayingGame()) {
            ActivateMainMenu();
        }
        else {
            ActivatePauseMenu();
        }
    }

    private void AutoSelectButton(GameObject panel, string buttonName) {
        GameObject buttonObject = panel.transform.Find(buttonName).gameObject;
        if (buttonObject != null) {
            Button button = buttonObject.GetComponent<Button>();

            button.OnSelect(null); //highlights the selected button
            eventSystem.SetSelectedGameObject(buttonObject);
        }
    }

    private bool IsPlayingGame() {
        return !(SceneManager.GetActiveScene().name.Equals("MainMenu"));
    }
}
