﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SoundPlayer : MonoBehaviour {

	public static SoundPlayer instance;

	public AudioSource musicAudioSource;
    public AudioClip musicTrackOne;
    public AudioClip musicTrackTwo;
    public AudioClip musicTrackThree;

    public AudioSource soundFXAudioSource;
    public AudioClip playerDeathSound;
    public AudioClip enemyDeathSound;

    public GameObject playerObj;

    private string sceneName;

	void Awake() {
		if (instance == null) {
			instance = this;
		}
		else if (instance != null) {
			Destroy(this);
		}
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

    void OnSceneLoaded(Scene arg0, LoadSceneMode arg1) {
		sceneName = SceneManager.GetActiveScene().name;
        Debug.Log("Level Name: " + sceneName);
	}

    public void PlayTrackOne() {
        musicAudioSource.clip = musicTrackOne;
        musicAudioSource.Play();
    }

    public void PlayTrackTwo() {
        musicAudioSource.clip = musicTrackTwo;
        musicAudioSource.Play();
    }

    public void PlayTrackThree() {
        musicAudioSource.clip = musicTrackThree;
        musicAudioSource.Play();
    }

    public void StopMusic() {
        musicAudioSource.Stop();
    }

    public void PlayPlayerDeathSound() {
        soundFXAudioSource.PlayOneShot(playerDeathSound);
    }

    public void PlayEnemyDeathSound() {
		soundFXAudioSource.PlayOneShot(enemyDeathSound);
	}

    public void SetMusicVolume(float value) {
        if (musicAudioSource != null) {
            musicAudioSource.volume = value;
            PlayerPrefs.SetFloat("Music Volume", value);
        }
    }

    public void SetSoundFXVolume(float value) {
        if (soundFXAudioSource != null) {
            soundFXAudioSource.volume = value;
            PlayerPrefs.SetFloat("SFX Volume", value);
        }
    }
}
