﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Enemy : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collider) {
        GameObject entity = collider.gameObject;
        if (entity.CompareTag("PlayerSword")) {
            Sword sword = entity.GetComponentInParent<Sword>();
            if (sword.isSlicing) {
                GameControl.instance.KillEnemy(gameObject);
                SoundPlayer.instance.PlayEnemyDeathSound();
                Destroy(gameObject);
            }
        }
    }
}
